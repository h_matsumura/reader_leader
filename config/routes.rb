Rails.application.routes.draw do
  namespace :admin do
    resources :users
    resources :books
    # end
    root to: 'users#index'
  end
  get 'static_pages/about'

  get 'login' => 'user_sessions#new', :as => :login
  post 'logout' => 'user_sessions#destroy', :as => :logout

  resources :users, except: [:index], shallow: true do
    resources :finished_books, only: [:index]
    resources :books, defaults: { format: 'html' }, except: [:show] do
      member do
        get 'really_destroy'
        patch 'restore'
      end
    end
  end

  resource :user_sessions, only: [:create, :new, :destroy]
  # [POST] "/user_sessions/new"
  resources :password_resets, only: [:create, :edit, :update]
  # get :login, to: 'user_sessions#new'
  # delete :logout, to: 'user_sessions#destroy'
  # resources :user_sessions, only: %i[create]
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  # http://c5meru.hatenablog.jp/entry/2018/07/29/200011
  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?
  root 'user_sessions#new'
end
