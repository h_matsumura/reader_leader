# 他の記事では cofig/environments/○○.rb で書いているが、他の設定も書いていて見にくいので、今回は config/initializers/ 配下で設定する。
# https://qiita.com/hirotakasasaki/items/ec2ca5c611ed69b5e85e
if Rails.env.production?
  ActionMailer::Base.delivery_method = :smtp
  ActionMailer::Base.smtp_settings = {
    address: 'smtp.gmail.com',
    domain: 'gmail.com',
    port: 587,
    user_name: Rails.application.credentials.gmail[:USER_NAME],
    password: Rails.application.credentials.gmail[:PASSWORD],
    authentication: 'plain',
    enable_starttls_auto: true
  }
elsif Rails.env.development?
  ActionMailer::Base.delivery_method = :letter_opener
else
  ActionMailer::Base.delivery_method = :test
end
