class User < ApplicationRecord
  # emailが空の場合があるのでif分岐
  before_save { self.email = email&.downcase }
  # before_save { email&.downcase! } # 破壊的再代入なのでfrozenerrorが出る
  authenticates_with_sorcery!
  # DBの一対多。これrails6ではいらなくなったのと違ったんか？
  # https://qiita.com/Ushinji/items/650fa295a3054d2fe582
  # 2020/08/11inverse_of追加
  has_many :books, inverse_of: :user, dependent: :delete_all
  # 普通にdependent: :destroyとすると、gemのせいで論理削除になってしまう。paranoiaじゃなくて次の世代のgemにすればよかった

  validates :password, length: { minimum: 3 }, if: -> { new_record? || changes[:crypted_password] }
  validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes[:crypted_password] }

  validates :user_name, uniqueness: true, presence: true, length: { maximum: 50 }
  # VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # VALID_EMAIL_REGEX =  /\A\S+@\S+\.\S+\z/
  # メルアド用の正規表現。コントローラでバリデーションするのは無理そうなのでビュー側で
  validates :email, length: { maximum: 255 }, uniqueness: true, allow_nil: true # 空白許容するけど入れるならユニークネスってできるのか？
  # , format: { with: VALID_EMAIL_REGEX }
end
