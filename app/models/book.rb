class Book < ApplicationRecord
  acts_as_paranoid
  belongs_to :user
  default_scope -> { order(:user_id, :priority) }
  validates :user_id, presence: true
  validates :title, presence: true, length: { maximum: 129 }
  # ロビンソン・クルーソーが129文字
  validate :when_stop_should_be_after_when_start

  def still_not_read?
    # 未読。読書中でもなく、かつ既読でもない
    # 完全な未読と中断を区別していない。yagni
    !reading? && !had_read?
  end

  def reading?
    when_start && !when_stop && !deleted_at
  end

  def had_read?
    # ただのシンタックスシュガー。二重否定でboolに変える
    !!deleted_at
  end

  private

  def when_stop_should_be_after_when_start
    return unless when_stop && when_start

    errors.add(:when_stop, 'は開始日より前には設定できません') if when_stop < when_start
  end
end
