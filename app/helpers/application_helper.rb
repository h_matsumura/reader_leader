module ApplicationHelper
  JPN_PRIORITIES = %w[最高 高 通常 低 最低].freeze

  def list_priorities
    JPN_PRIORITIES.map.with_index { |jpn, index| [jpn, index] }
  end

  def jpn_priority(number_priority)
    JPN_PRIORITIES[number_priority] if number_priority.is_a?(Integer)
  end
end
