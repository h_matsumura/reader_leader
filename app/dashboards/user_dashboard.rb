require 'administrate/base_dashboard'

class UserDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    books: Field::HasMany,
    id: Field::Number,
    user_name: Field::String,
    email: Field::String,
    crypted_password: Field::String,
    salt: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    admin: Field::Boolean,
    reset_password_token: Field::String,
    reset_password_token_expires_at: Field::DateTime,
    reset_password_email_sent_at: Field::DateTime,
    access_count_to_reset_password_page: Field::Number
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[id user_name books email admin].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  # SHOW_PAGE_ATTRIBUTES = [:books, :id, :user_name, :email, :crypted_password, :salt, :created_at, :updated_at, :admin, :reset_password_token, :reset_password_token_expires_at, :reset_password_email_sent_at, :access_count_to_reset_password_page].freeze
  # booksいらなくね？
  SHOW_PAGE_ATTRIBUTES = %i[id user_name email crypted_password salt created_at updated_at admin reset_password_token reset_password_token_expires_at reset_password_email_sent_at access_count_to_reset_password_page].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  # FORM_ATTRIBUTES = [:books, :user_name, :email, :crypted_password, :salt, :admin, :reset_password_token, :reset_password_token_expires_at, :reset_password_email_sent_at, :access_count_to_reset_password_page].freeze
  # booksいらなくね？
  FORM_ATTRIBUTES = %i[user_name email crypted_password salt admin reset_password_token reset_password_token_expires_at reset_password_email_sent_at access_count_to_reset_password_page].freeze

  # COLLECTION_FILTERS
  # a hash that defines filters that can be used while searching via the search
  # field of the dashboard.
  #
  # For example to add an option to search for open resources by typing "open:"
  # in the search field:
  #
  #   COLLECTION_FILTERS = {
  #     open: ->(resources) { resources.where(open: true) }
  #   }.freeze
  COLLECTION_FILTERS = {}.freeze

  # Overwrite this method to customize how users are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(user)
  #   "User ##{user.id}"
  # end
end
