// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")

require("jquery")


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)


import 'bootstrap'
import '../src/application.scss'

// honokaをyarnでインストールするとnode_modules以下に配置されるが、Rails5ではデフォルトではnode_modulesを読まない
// rails6では読むのかもしれないが、不安なので一応インポートしておく
// https://qiita.com/akicho8/items/a23d380a3382bdf1aef4
// https://thr3a.hatenablog.com/entry/20181028/1540678386
import "bootstrap-honoka/dist/css/bootstrap.min.css"
import "bootstrap-honoka/dist/js/bootstrap.min.js"

require("trix")
require("@rails/actiontext");

import Rails from '@rails/ujs';
Rails.start();
