# app/controllers/user_sessions_controller.rb
class UserSessionsController < ApplicationController
  skip_before_action :require_login, only: %i[new create]

  def new
    @user = User.new
  end

  def create
    @user = login(params[:user_name], params[:password])
    if @user
      redirect_back_or_to(user_books_path(@user), notice: 'ログインしました。')
      # 昔はredirect_back_or_to(@user, notice: 'Login successfull.')だったが、ユーザーのインデックスページはなくした
    else
      flash.now[:alert] = 'ログインに失敗しました。'
      render action: 'new'
    end
  end

  def destroy
    logout
    redirect_to(root_url, notice: 'Logged out!')
  end
end
