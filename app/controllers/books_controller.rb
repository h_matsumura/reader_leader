class BooksController < ApplicationController
  before_action :correct_book_owner, except: %i[index new create]
  before_action :correct_user, only: %i[index new create]

  def index
    # @user = User.find(params[:user_id])
    @q = @user.books.ransack(params[:q])
    @books = @q.result # (distinct: true)は複数の検索結果をまとめる
  end

  def edit
    @book = Book.with_deleted.find(params[:id])
  end

  def update
    @book = Book.with_deleted.find(params[:id])
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to current_user, notice: 'Book was successfully updated.' }
        format.json { render :show, status: :ok, location: current_user }
      else
        format.html { render :edit }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  def new
    # @user = User.find(params[:user_id])
    @book = Book.new
  end

  def create
    @book = Book.new(book_params)

    respond_to do |format|
      if @book.save
        format.html { redirect_to current_user, notice: 'Book was successfully created.' }
        format.json { render :show, status: :created, location: current_user }
      else
        format.html { render :new }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    respond_to do |format|
      format.html { redirect_to current_user, notice: 'Book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def really_destroy
    @book = Book.with_deleted.find(params[:id])
    @book.really_destroy!
    respond_to do |format|
      format.html { redirect_to current_user, notice: 'Book was successfully really destroyed.' }
      format.json { head :no_content }
    end
  end

  def restore
    @book = Book.only_deleted.find(params[:id])
    @book.restore
    respond_to do |format|
      format.html { redirect_to current_user, notice: 'Book was successfully restored.' }
      format.json { head :no_content }
    end
  end
  # Override this method to specify custom lookup behavior.
  # This will be used to set the resource for the `show`, `edit`, and `update`
  # actions.
  #
  # def find_resource(param)
  #   Foo.find_by!(slug: param)
  # end

  # The result of this lookup will be available as `requested_resource`

  # Override this if you have certain roles that require a subset
  # this will be used to set the records shown on the `index` action.
  #
  # def scoped_resource
  #   if current_user.super_admin?
  #     resource_class
  #   else
  #     resource_class.with_less_stuff
  #   end
  # end

  # Override `resource_params` if you want to transform the submitted
  # data before it's persisted. For example, the following would turn all
  # empty values into nil values. It uses other APIs such as `resource_class`
  # and `dashboard`:
  #
  # def resource_params
  #   params.require(resource_class.model_name.param_key).
  #     permit(dashboard.permitted_attributes).
  #     transform_values { |value| value == "" ? nil : value }
  # end

  # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
  # for more information
  private

  def book_params
    params.require(:book).permit(:title, :priority, :when_start, :when_stop, :deleted_at).merge(user_id: current_user.id)
  end

  def correct_user
    @user = User.find(params[:user_id])
    redirect_to(root_url) unless @user == current_user
  end

  def correct_book_owner
    redirect_to(root_url) unless Book.with_deleted.find(params[:id]).user_id == current_user.id
  end
end
