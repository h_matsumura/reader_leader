class ApplicationController < ActionController::Base
  before_action :require_login
  helper_method :current_user

  protected

  def not_authenticated
    redirect_to login_path, alert: 'Please login first'
  end

  private

  def current_user
    return unless session[:user_id]

    # @current_user ||= User.find(session[:user_id])
    User.find(session[:user_id])
  end

  def correct_user
    raise NotImplementedError,
          "#{self.class}では、correct_userメソッドが未実装です。"
  end
end
