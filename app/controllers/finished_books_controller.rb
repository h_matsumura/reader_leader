class FinishedBooksController < ApplicationController
  before_action :correct_user

  def index; end

  private

  def correct_user
    # @user = User.find(params[:id])
    @user = User.find(params[:user_id])
    redirect_to(login_url) unless @user == current_user
  end
end
