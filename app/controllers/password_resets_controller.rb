# app/controllers/password_resets_controller.rb
class PasswordResetsController < ApplicationController
  # In Rails 5 and above, this will raise an error if
  # before_action :require_login
  # is not declared in your ApplicationController.
  skip_before_action :require_login

  # request password reset.
  # you get here when the user entered their email in the reset password form and submitted it.
  def create
    # This line sends an email to the user with instructions on how to reset their password (a url with a random token)
    if User.find_by(email: params[:email])&.deliver_reset_password_instructions!
      # Tell the user instructions have been sent whether or not email was found.
      redirect_to(root_path, notice: 'パスワード再設定メールを送信しました。')
    else # アドレスが存在しなかった場合
      redirect_to(root_path, alert: 'メールアドレスが見つかりません。パスワード再設定メール送信に失敗しました。')
    end
  end

  # This is the reset password form.
  def edit
    @token = params[:id]
    @user = User.load_from_reset_password_token(params[:id])

    return if @user.present? # ガード節。ここから下はuserがblankだった時のエラートラップ

    not_authenticated
    nil
  end

  # This action fires when the user has sent the reset password form.
  def update
    @token = params[:id]
    @user = User.load_from_reset_password_token(params[:id])

    if @user.blank?
      not_authenticated
      return
    end

    # the next line makes the password confirmation validation work
    @user.password_confirmation = params[:user][:password_confirmation]
    # the next line clears the temporary token and updates the password
    @user.change_password(params[:user][:password]) ? redirect_to(root_path, notice: 'パスワード再設定に成功しました。') : (render action: 'edit')
  end
end
