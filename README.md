# ReaderLeader

積ん読状態の本に優先順位をつけて整理するサイトです。

「読んでる途中だけど、新しい本読みたいから一度中断」にも対応しています。

レスポンシブ対応しているのでスマホからもご確認いただけます。 

![ユーザー画面のイメージ画像です。](https://bitbucket.org/h_matsumura/reader_leader/raw/126942ce75283628a83bbe617bc90ca329f45217/0.16.23.png "ユーザー画面"){width=640 height=480}

## URL

[reader-leader](https://reader-leader.herokuapp.com)

とりあえず試してみたい人は、ゲストユーザーとしてログインできます。

## 使用技術

* Ruby 2.6.3
* Ruby on Rails 6.1.1
* PostgreSQL 12.5
* Heroku
* Puma
* minitest
* capybara

## 機能

- ユーザーCRUD機能
- ログイン・アウト機能
- パスワードリセット機能
- 管理ユーザー追加機能
- 書籍CRUD機能
- 書籍論理削除機能
- 書籍検索機能

![管理者画面のイメージ画像です。](https://bitbucket.org/h_matsumura/reader_leader/raw/126942ce75283628a83bbe617bc90ca329f45217/0.17.53.png "管理者画面"){width=640 height=480}

## テスト

1. minitest(controller)
2. minitest(mailer)
3. minitest(model)
4. minitest(system)

## デプロイ

Heroku

## 作者

Hal_Mat(https://zenn.dev/hal_mat)

mail to: (hmatsumura120@gmail.com)

## 今後の課題
1. サイトマップ、メタタグ、パンくずリストの整備
2. cssの整備