# -*- encoding: utf-8 -*-
# stub: administrate-field-trix 0.0.3 ruby lib

Gem::Specification.new do |s|
  s.name = "administrate-field-trix".freeze
  s.version = "0.0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Ian Walter".freeze, "Noah Settersten".freeze]
  s.date = "2018-08-31"
  s.description = "A plugin to use the Trix WYSIWYG editor in Administrate".freeze
  s.email = ["public@iankwalter.com".freeze]
  s.homepage = "https://github.com/appjumpstart/administrate-field-trix".freeze
  s.licenses = ["ISC".freeze]
  s.rubygems_version = "3.2.5".freeze
  s.summary = "A plugin to use the Trix WYSIWYG editor in Administrate".freeze

  s.installed_by_version = "3.2.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<administrate>.freeze, ["< 1.0.0"])
    s.add_runtime_dependency(%q<rails>.freeze, [">= 4.1"])
    s.add_runtime_dependency(%q<trix-rails>.freeze, [">= 0.11.4.1"])
  else
    s.add_dependency(%q<administrate>.freeze, ["< 1.0.0"])
    s.add_dependency(%q<rails>.freeze, [">= 4.1"])
    s.add_dependency(%q<trix-rails>.freeze, [">= 0.11.4.1"])
  end
end
