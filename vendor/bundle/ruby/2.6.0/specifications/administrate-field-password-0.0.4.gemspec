# -*- encoding: utf-8 -*-
# stub: administrate-field-password 0.0.4 ruby lib

Gem::Specification.new do |s|
  s.name = "administrate-field-password".freeze
  s.version = "0.0.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Adrian Rangel".freeze]
  s.date = "2017-01-16"
  s.description = "Easily add Password fields to your administrate views".freeze
  s.email = ["adrian@disruptiveangels.com".freeze]
  s.homepage = "https://github.com/disruptiveangels/administrate-field-password".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "3.2.5".freeze
  s.summary = "Add Password fields to Administrate".freeze

  s.installed_by_version = "3.2.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<administrate>.freeze, [">= 0"])
  else
    s.add_dependency(%q<administrate>.freeze, [">= 0"])
  end
end
