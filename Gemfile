source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails',  '>= 6.0.3'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 4.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'actiontext'
# Use Active Storage variant
gem 'image_processing', '~> 1.2'
# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false
gem 'sorcery'
gem 'administrate'
# https://blog.123abcsoft.com/2020/02/25/rails6-入門/
gem 'administrate-field-trix'
gem 'trix-rails', require: 'trix'
gem 'faker'
gem 'administrate-field-password'
# 論理削除
gem 'paranoia'
# 検索機能
# submodule使うやつはなんかエラー出る。2021/01/15変更
# gem 'ransack', github: 'activerecord-hackery/ransack'
gem 'ransack'
# 2021/01/29追加
# https://qiita.com/takecian/items/9f591096bae88187837e
gem 'rails-i18n'

group :development, :test do
  gem 'sqlite3', '1.4.1'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # 開発環境で試すたびに実際にメールが送られてくるのは超ウザい
  gem "letter_opener"
  gem "factory_bot_rails"
  # letteropenerはcloud9にブラウザが入っていないので使えない。
  # letteropenerwebってletter_openerに依存してるのだろうか？　消していい？
  gem 'letter_opener_web', '~> 1.0'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'rubocop', require: false
  gem 'rubocop-performance'
  gem 'rubocop-rails'
  gem 'rubocop-rspec'
  # 2020/08/11precommit追加.rubocop -aするようにしたけどこれほんとに動いてんのか？「きゃまなかのブログ」で検索
  # gem 'pre-commit', require: false
  # 2021/01/13overcommitに変更。precommitだとディレクトリ除外が効かない
  gem 'overcommit'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  # gem 'capybara', '>= 2.15'
  # gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  # gem 'webdrivers'
  gem 'capybara',                 '3.28.0'
  gem 'rails-controller-testing', '1.0.4'
  gem 'selenium-webdriver',       '~> 3.142.4'
  gem 'webdrivers',               '4.1.2'
  # 2020/05/26現在、minitestは新しすぎるとエラー
  # https://github.com/rails/rails/commit/268a099b1b8ad6aa58e52a100d4201f253663feb
  gem 'guard',                    '2.16.2'
  gem 'guard-minitest',           '2.4.6'
  gem 'minitest',                 '< 5.14.0'
  gem 'minitest-reporters',       '1.3.8'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
# gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

group :production do
  gem 'pg'
end
