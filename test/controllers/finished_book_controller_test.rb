require 'test_helper'

class FinishedBookControllerTest < ActionDispatch::IntegrationTest
  def setup
    @book_owner = FactoryBot.create(:user)
    @other_user = FactoryBot.create(:user)
  end

  test '未ログインではアクセス不可' do
    get user_finished_books_path(@book_owner)
    assert_redirected_to login_path
  end
  test '本人のログインならアクセス可' do
    sign_in_as @book_owner
    get user_finished_books_path(@book_owner)
    assert_response :success
  end
  test '他人のログインではアクセス不可' do
    sign_in_as @other_user
    get user_finished_books_path(@book_owner)
    assert_redirected_to login_path
  end
end
