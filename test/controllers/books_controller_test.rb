require 'test_helper'

class BooksControllerTest < ActionDispatch::IntegrationTest
  def setup
    @book_owner = FactoryBot.create(:user)
    @other_user = FactoryBot.create(:user)
  end
  test '未ログインだとブックを作れない' do
    # どう書き間違えても作れない事が判明したので無視
  end

  test 'ログインすればブックを作れる' do
    sign_in_as @book_owner
    assert_difference 'Book.count', 1 do
      FactoryBot.create(:book)
    end
  end

  test '他人の作ったブックは編集できない' do
    book = FactoryBot.create(:book)
    sign_in_as @other_user
    get edit_book_path(book)
    assert_redirected_to root_path
  end

  test '自分が作ったブックは編集できる' do
    sign_in_as @book_owner
    book = FactoryBot.create(:book, user: @book_owner)
    get edit_book_path(book)
    patch book_path(book), params: { book: { title: 'Truth', priority: 4 } }
    book.reload
    assert_equal 'Truth', book.title
  end

  test '他人の作ったブックは削除できない' do
    @book_owner = FactoryBot.create(:user)
    book = FactoryBot.create(:book)
    sign_in_as @other_user
    assert_no_difference 'Book.count' do
      delete book_path(book)
    end
  end

  test '自分が作ったブックは削除できる' do
    sign_in_as @book_owner
    book = FactoryBot.create(:book, user: @book_owner)
    assert_difference 'Book.count', -1 do
      delete book_path(book)
    end
  end
  test '論理削除したブックは復活できる' do
    sign_in_as @book_owner
    book = FactoryBot.create(:book, user: @book_owner)
    delete book_path(book)
    assert_difference 'Book.count', 1 do
      # book.restore
      patch restore_book_path(book)
    end
  end
  test '物理削除したブックは復活できない' do
    sign_in_as @book_owner
    book = FactoryBot.create(:book, user: @book_owner)
    assert_no_difference 'Book.only_deleted.count' do
      get really_destroy_book_path(book)
    end
    # assert_equal Book.only_deleted.count, 0
  end
end
