require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  setup do
    @user = FactoryBot.create(:user)
    @other_user = FactoryBot.create(:user)
  end

  test 'users indexは見えないように' do
    assert_raises(ActionController::RoutingError) { get users_url }
  end

  test 'should get new' do
    get new_user_url
    assert_response :success
  end

  test 'should create user' do
    assert_difference 'User.count',1 do
      post users_url, params: { user: { user_name: 'third_user', password: 'gluplup', password_confirmation: 'gluplup' } }
      # post users_url, params: { user: { name: @user.name, password_digest: @user.password_digest } }
    end
    assert_redirected_to user_url(User.last)
  end

  test 'should get edit' do
    sign_in_as @user
    get edit_user_url(@user)
    assert_response :success
  end

  test '他人のログインではeditできない' do
    sign_in_as @user
    get edit_user_url(@other_user)
    assert_response :redirect
  end

  test 'should update user' do
    sign_in_as @user
    patch user_url(@user), params: { user: { user_name: 'Truth', email: 'example@gmail.com' } }
    # patch user_url(@user), params: { user: { user_name: 'Truth', salt: @user.salt, crypted_password: @user.crypted_password } }
    # patch user_url(@user), params: { user: { name: @user.name, password_digest: @user.password_digest } }
    assert_redirected_to user_url(@user)
    @user.reload
    assert_equal 'Truth', @user.user_name
    assert_equal 'example@gmail.com', @user.email
  end

  # test 'cannot update other user' do
  #   sign_in_as @user
  #   patch user_url(@other_user), params: { user: { user_name: 'Dummy', salt: @other_user.salt, crypted_password: @other_user.crypted_password } }
  #   assert_not_equal @other_user.user_name, 'Dummy'
  # end



  # 管理者権限指定を入れたため不可
  # test 'should destroy user' do
  #   assert_difference('User.count', -1) do
  #     delete user_url(@user)
  #   end
  #   assert_redirected_to users_url
  # end

  # test "should redirect edit when not logged in" do
  #   get edit_user_path(@user)
  #   assert_not flash.empty?
  #   assert_redirected_to login_url
  # end

  # test "should redirect update when not logged in" do
  #   patch user_path(@user), params: { user: { user_name: @user.user_name, salt: @user.salt, crypted_password: @user.crypted_password } }
  #   assert_not flash.empty?
  #   assert_redirected_to login_url
  # end

  # 管理者権限指定を入れたため不可
  # test 'should show user' do
  #   get user_url(@user)
  #   assert_response :success
  # end
end
