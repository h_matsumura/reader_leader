FactoryBot.define do
  factory :book do
    user
    title { Faker::Book.title }
    # sequence(:when_start) { |i| rand(1..30).days.from_now if i.even?}
    # sequence(:when_stop) { |i| when_start + rand(1..30).days.from_now if (i % 4 == 0)}
  end
end
