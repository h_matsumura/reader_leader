FactoryBot.define do
  factory :user, aliases: [:owner] do
    sequence(:user_name) { |i| "testuser#{i}"}
    sequence(:email) { |i| "example#{i}@example.com" if i.odd?}
    password  { "password" }
    password_confirmation { "password" }
  end
end
