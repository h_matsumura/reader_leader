module SignInHelper
  include Sorcery::TestHelpers::Rails::Integration
  attr_reader :current_user

  def sign_in_as(user)
    if respond_to?(:visit)
      visit login_url
      fill_in 'User name', with: user.user_name
      fill_in 'Password', with: 'password'
      # click_on "Login"
      click_button('Login', exact: false)
    elsif respond_to?(:get)
      post "/user_sessions", params: { user_name: user.user_name, password: 'password' }
    else
      raise NotImplementError
    end
    @current_user = user
  end
end

class ActionDispatch::IntegrationTest
  include SignInHelper
end
