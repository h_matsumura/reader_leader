require 'application_system_test_case'

class UsersTest < ApplicationSystemTestCase
  # include Sorcery::TestHelpers::Rails::Integration
  # include Sorcery::TestHelpers::Rails::Controller

  # setup do
  # end

  test '個人ページへ' do
    user=FactoryBot.create(:user)
    visit user_url(user)
    assert_selector 'h1', text: "ログイン"
    sign_in_as(user)
    visit user_url(user)
    assert_selector 'h1', text: "こんにちは、#{user.user_name}さん"
  end

  test 'ログインページからアカウント作成ページへ' do
    visit login_url
    assert_selector 'h1', text: "ログイン"
    click_on 'アカウント作成'
    assert_selector 'h1', text: "新規ユーザー登録"
  end

  # ゲストユーザーページのテストはなし。
  # ゲストユーザーはDBにハードコーディングしているので、
  # ゲストユーザーをクリエイトしてからゲストユーザーボタンをテストするという無意味テストになってしまう

  test 'ユーザーページの開始ボタン・読了ボタン/完全削除ボタン' do
    user = FactoryBot.create(:user)
    FactoryBot.create(:book, title: "testtestest", user_id: user.id)
    sign_in_as(user)
    visit user_url(user)
    page.has_button?('開始')
    click_on '開始'
    page.has_button?('読了')
    click_on '読了'
    click_on "読了済みリストを見る"
    assert_text '読了済みリスト'
    page.has_button?('完全削除')
    click_on '完全削除'
    page.driver.browser.switch_to.alert.accept
    page.has_no_button?('完全削除')
  end

  # 基本機能はgem実装なのでテストなし
  # test 'creating a User' do
  # end

  # test 'updating a User' do
  # end

  # test 'destroying a User' do
  # end

end
