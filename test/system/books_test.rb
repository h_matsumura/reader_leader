require 'application_system_test_case'

class BooksTest < ApplicationSystemTestCase
  include Sorcery::TestHelpers::Rails::Controller
  include Sorcery::TestHelpers::Rails::Integration

  setup do
    @user = FactoryBot.create(:user)
  end

  test '個人ページを開く' do
    book = FactoryBot.create(:book, user_id: @user.id)
    visit root_url
    sign_in_as(@user)
    visit user_url(@user)
    page.has_content?(book.title)
    click_on ('ログアウトする')
  end

  test 'Bookを登録する' do
    sign_in_as(@user)
    visit user_url(@user)
    click_on '新しく本を登録する'

    fill_in 'Title', with: 'testtestest'
    select '最高', from: 'book[priority]'
    click_on '書籍情報を保存する'

    assert_text 'Book was successfully created'
    click_on ('ログアウトする')
  end

  test '個人ページからBookをupdateする' do
    book = FactoryBot.create(:book, user_id: @user.id)
    visit root_url
    sign_in_as(@user)
    visit user_url(@user)
    click_on book.title

    fill_in 'Title', with: 'testtestest'
    select '最高', from: 'book[priority]'
    click_on '書籍情報を保存する'

    assert_text 'Book was successfully updated.'
  end

  test '読書開始、読了、中断、削除、復旧ボタン' do
    book_start_stop_end_restore = FactoryBot.create(:book, user_id: @user.id)
    book_really_destroy1 = FactoryBot.create(:book, user_id: @user.id)
    book_really_destroy2 = FactoryBot.create(:book, user_id: @user.id, when_start: Date.today, when_stop: Date.today)
    visit root_url
    sign_in_as(@user)
    visit user_url(@user)

    click_on book_start_stop_end_restore.title
    click_on '開始'
    assert_text 'Book was successfully updated.'

    click_on book_start_stop_end_restore.title
    click_on '中断'
    assert_text 'Book was successfully updated.'

    click_on book_start_stop_end_restore.title
    click_on '開始'
    click_on book_start_stop_end_restore.title
    click_on '読了'
    assert_text 'Book was successfully destroyed.'

    click_on book_really_destroy1.title
    click_on '完全削除'
    page.driver.browser.switch_to.alert.accept
    assert_text 'Book was successfully really destroyed.'

    click_on "読了済みリストを見る"
    click_on book_start_stop_end_restore.title
    click_on '復旧'
    assert_text 'Book was successfully restored.'

    click_on book_really_destroy2.title
    click_on '完全削除'
    page.driver.browser.switch_to.alert.accept
    assert_text 'Book was successfully really destroyed.'
  end
  # test '個人ページからbookをdestroyする' do
  #   visit books_url
  #   page.accept_confirm do
  #     click_on 'Destroy', match: :first
  #   end

  #   assert_text 'Book was successfully destroyed'
  # end
end
