require 'application_system_test_case'

class StaticPagesTest < ApplicationSystemTestCase
  test '/ページを表示' do
    visit root_url
    assert_selector 'h1', text: 'ログイン'
  end
end
