require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:one)
  end

  # test 'login with invalid information' do
  #   get login_path
  #   assert_template 'user_sessions/new'
  #   get login_path, params: { user_name: '', password: '' }
  #   # post login_path, params: { user_name: "", password: ""  }
  #   # post login_path, params: { session: { user_name: "", password: "" } }
  #   assert_template 'user_sessions/new'
  #   # assert_not flash.empty?
  #   # ログイン失敗のフラッシュテスト。ログインを失敗させる方法がわからない。
  #   get root_path
  #   assert flash.empty?
  # end

  # ログイン成功テスト。gemで実装してるので不要。というかログインする方法が不明
  # test "login with valid information" do
  #   get login_path
  #   post login_path, params: { user_name: @user.user_name, password: 'secret' }
  #   assert_redirected_to @user
  #   follow_redirect!
  #   assert_template 'users/show'
  #   assert_select "a[href=?]", login_path, count: 0
  #   assert_select "a[href=?]", logout_path
  #   assert_select "a[href=?]", user_path(@user)
  # end
end
