require 'test_helper'

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  # driven_by :selenium, using: :chrome, screen_size: [1400, 1400]
  # chromeが入ってなくて半殺しにされた。driver入れてないけどいいのかこれ？
  # https://kvonhorn.github.io/2017/05/04/installing_headless_chrome_on_c9.html
  driven_by :selenium, using: :headless_chrome, screen_size: [1400, 1400]

  include SignInHelper
end
