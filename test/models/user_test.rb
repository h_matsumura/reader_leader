require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(user_name: 'Example User',
                     password: 'foobar', password_confirmation: 'foobar')
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.user_name = "     "
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.user_name = "a" * 51
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end

  # 正規表現バリデーションはビュー側で
  # test "email validation should accept valid addresses" do
  #   valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
  #                       first.last@foo.jp alice+bob@baz.cn]
  #   valid_addresses.each do |valid_address|
  #     @user.email = valid_address
  #     assert @user.valid?, "#{valid_address.inspect} should be valid"
  #   end
  # end

  # test "email validation should reject invalid addresses" do
  #   invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
  #                         foo@bar_baz.com foo@bar+baz.com]
  #   invalid_addresses.each do |invalid_address|
  #     @user.email = invalid_address
  #     assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
  #   end
  # end

  test "user name addresses should be unique" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 2
    assert_not @user.valid?
  end

  test 'associated books should be destroyed' do
    @user.save
    @user.books.create!(title: 'Lorem ipsum')
    assert_difference 'Book.count', -1 do
      @user.destroy
    end
  end

  # test 'オーナーが消えたらブックは復活できない' do
  # modelに書け
  #   delete_owner = FactoryBot.create(:user)
  #   sign_in_as delete_owner
  #   book = FactoryBot.create(:book, user: delete_owner)
  #   assert_difference 'Book.count', -1 do
  #     delete user_path(delete_owner)
  #   end
  #   assert_nil book
  # end

end
