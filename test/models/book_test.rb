require 'test_helper'

class BookTest < ActiveSupport::TestCase
  test 'start_should_be_before_end' do
    when_start = rand(1..30).days.from_now
    when_stop = when_start + rand(1..5).days
    book = FactoryBot.build(:book, when_start: when_start, when_stop: when_stop)
    book.valid?
    assert_empty(book.errors[:when_stop])
    # assertにvalid?　を直接つなげる事はなぜかできない。いつか調べる
    # assert book.valid?
  end
  test 'start_should_not_be_after_end' do
    when_start = rand(10..30).days.from_now
    when_stop = when_start - rand(1..5).days
    book = FactoryBot.build(:book, when_start: when_start, when_stop: when_stop)
    book.valid?
    assert_not_empty(book.errors[:when_stop])
    # assertにvalid?　を直接つなげる事はなぜかできない。いつか調べる
    # assert_not book.valid?
  end
  test 'user_idを消す' do
    book = FactoryBot.build(:book)
    book.user_id = nil
    assert book.invalid?
  end
  test "title should be present" do
    book = FactoryBot.build(:book)
    book.title =  "   "
    assert book.invalid?
  end
  test "title should be at most 129 characters" do
    book = FactoryBot.build(:book)
    book.title =  "a" * 130
    assert book.invalid?
  end
  test "reading?" do
    book = FactoryBot.build(:book, when_start: nil, when_stop: nil)
    assert_not book.reading?
    book.when_start = Date.new(1999, 9, 9)
    assert book.reading?
  end
end
