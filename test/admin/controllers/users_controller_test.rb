require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  # 管理者ならアクセス可、管理者じゃないならアクセス不可、ログインしてなくてもアクセス不可

  test '非ログインなら不可視' do
    get admin_root_path
    assert_response :redirect
  end
  test '非管理者でログインしたら不可視' do
    not_admin_user = FactoryBot.create(:user)
    sign_in_as not_admin_user
    get admin_root_path
    assert_response :redirect
  end
  test '管理者でログインしたら成功' do
    admin_user = FactoryBot.create(:user, admin: true)
    sign_in_as admin_user
    get admin_root_path
    assert_response :success
  end
end
