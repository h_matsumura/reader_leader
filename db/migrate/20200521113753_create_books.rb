class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :title, null: false
      t.integer :priority, null: true
      t.date :when_start, null: true
      t.date :when_stop, null: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
    # add関連性うんぬんは不要
    # https://teratail.com/questions/91983
  end
end
