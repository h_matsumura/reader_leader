class SorceryCore < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :user_name,        null: false
      t.string :email,            null: true
      t.string :crypted_password
      t.string :salt

      t.timestamps null: false
    end

    add_index :users, :user_name, unique: true
    # メールの方にユニークインデックスをつけ忘れた。
    # 後からつける方法が分からないのでmigrateファイルを2021/01/07増やした
    # https://forest-valley17.hatenablog.com/entry/2018/09/12/163849
  end
end
