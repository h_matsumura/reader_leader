class AddIndexToUsersEmail < ActiveRecord::Migration[6.1]
  def change
    add_index :users, :email, unique: true
    # https://forest-valley17.hatenablog.com/entry/2018/09/12/163849
  end
end
