# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(user_name: 'Example User',
             password: 'foobar',
             password_confirmation: 'foobar')

9.times do |n|
  user_name = Faker::Name.name
  email = Faker::Internet.safe_email(name: "sample_#{n}") if n.odd? # nが奇数ならメルアドあり、偶数なら空欄
  password = 'password'
  User.create!(user_name: user_name,
               email: email,
               password: password,
               password_confirmation: password)
end

admin_email = Rails.application.credentials.admin[:EMAIL] # 直接パラメータに書くと小文字変換で怒らせる

User.create!(user_name: Rails.application.credentials.admin[:USER_NAME],
             email: admin_email,
             password: Rails.application.credentials.admin[:PASSWORD],
             password_confirmation: Rails.application.credentials.admin[:PASSWORD],
             admin: true)

users = User.order(:created_at).take(6)
50.times do
  title = Faker::Book.title
  priority = rand(5) # 0から4まで
  users.each { |user| user.books.create!(title: title, priority: priority) }
end
